<button type="button" class="btn btn-info btn-lg anterior" onclick="pagina3()">
	Anterior
</button>

<div class="row" style="padding-bottom: 50px;">
	<div class="col-sm-12">

		<input name="pregunta[1]" id="pregunta1" type="hidden" value=""/>
		<input name="pregunta[2]" id="pregunta2" type="hidden" value=""/>
		<input name="pregunta[3]" id="pregunta3" type="hidden" value=""/>
		
		<div class="pregunta4">
			
			<h1><?php echo $pregunta4 -> texto; ?></h1>

			<?php foreach($pregunta4->alternativas as $alternativa)
			{
			?>
				<div class="radio">
					<label>
						<input type="radio" name="pregunta[4]" id="alternativa<?php echo $alternativa -> id; ?>" value="<?php echo $alternativa -> id; ?>">
						<h3><?php echo $alternativa -> texto; ?></h3></label>
				</div>
			<?php } ?>
			
		</div>
		
		<div class="pregunta5">
			
			<h1><?php echo $pregunta5 -> texto; ?></h1>

			<?php foreach($pregunta5->alternativas as $alternativa)
			{
			?>
				<div class="radio">
					<label>
						<input type="radio" name="pregunta[5]" id="alternativa<?php echo $alternativa -> id; ?>" value="<?php echo $alternativa -> id; ?>">
						<h3><?php echo $alternativa -> texto; ?></h3></label>
				</div>
			<?php } ?>
			
		</div>
		
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<a href="#" class="btn btn-success btn-lg" id ="submit-button">
					Terminar encuesta
				</a>
			</div>
		</div>
	</div>
</div>
<script>

		$('#pregunta1').val($('#primero').val());
		$('#pregunta2').val($('#segundo').val());
		$('#pregunta3').val($('#tercero').val());
		$(document).scrollTop(0);
		
		
		function pagina3()
		{
			$('#pagina').load('<?php echo Yii::app() -> createAbsoluteUrl('site/pagina3'); ?>?primer='+$('#primero').val()+'&segundo='+$('#segundo').val());
		}

		$('#submit-button').click(function(event)
		{
			event.preventDefault();
			var submit=false;
			if(($('form input[name="pregunta[4]"]:checked').val() == null || $('form input[name="pregunta[4]"]:checked').val() == "" ) || ($('form input[name="pregunta[5]"]:checked').val()==null || $('form input[name="pregunta[5]"]:checked').val() == "" ))
			{
				submit=false;
			}
			else submit=true;
	
			if(!submit)alert("Por favor, responda todas las preguntas");
			else $('#form_id').submit();
		});
</script>