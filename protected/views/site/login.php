<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.mask.min.js',CClientScript::POS_END); ?>
<div class="row">
    <div class="col-sm-offset-1 col-sm-10"><h1>Para responder la encuesta necesita ingresar su rut</h1></div>
</div>

<div class="row login">
    <div class="form-horizontal" role="form">
        <div class="form-group" id="control-rut">
            <br>
            <label for="rut" class="col-sm-offset-1 col-sm-1 control-label">Rut</label>
            <div class="col-sm-3">
                <input class="form-control rut" id="rut" nombre="rut" placeholder="Ej: 12345678-9" maxlength="12" onkeyup="cambioRut(event)">
            </div>
            <span class="help-block"></span>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-3">
                <br>
                <a href="#" class="btn btn-info btn-lg" onclick="validarRut()">
                    Responder Encuesta
                </a>
                <br>
                &nbsp;
            </div>
        </div>
    </div>
</div>

<div class="clear">
    <br>
</div>

<script>
    $(document).ready(function () {
        $('.rut').mask('000.000.000-A', {reverse: true});
    });

    function cambioRut(e)
    {
        if (e.which == 13) {
            validarRut();
            e.preventDefault();
        }

        if ($("#control-rut").hasClass("has-error"))
        {
            $("#control-rut").removeClass("has-error");
            $(".help-block").html("");
        }
    }

    function validarRut() {
        var rutSinDv = 0;
        var dv = 0;
        var rut = $('#rut').val().toLowerCase();
        if (rut == null || $.trim(rut) == "")
        {
            rutInvalido();
            return;
        }
        rut = rut.split('.').join("");
        sinGuion = rut.split('-');

        if (sinGuion.length == 2) {
            rutSinDv = sinGuion[0];
            dv = sinGuion[1];
        } else if (sinGuion.length == 1) {
            rutSinDv = rut.substring(0, rut.length - 1);
            dv = rut.substring(rut.length - 1, rut.length);
        } else
        {
            rutInvalido();
            return;
        }

        if (rutSinDv.length < 6 && rutSinDv.length > 8)
        {
            rutInvalido();
            return;
        }
        if (getDv(rutSinDv) != dv)
        {
            rutInvalido();
            return;
        }

        //Submit formulario o pasar al segundo paso
        rutValido(rutSinDv + dv);
    }

    function rutValido(rut)
    {
        $("#control-rut").addClass("has-success");
        $(".help-block").html("Rut Válido");
        window.location.href = "<?php echo Yii::app()->createAbsoluteUrl(''); ?>?rut=" + rut

    }

    function rutInvalido()
    {
        $("#control-rut").addClass("has-error");
        $(".help-block").html("Rut Inválido");
    }

    function getDv(T) {
        var M = 0, S = 1;
        for (; T; T = Math.floor(T / 10))
            S = (S + T % 10 * (9 - M++ % 6)) % 11;
        return S ? S - 1 : 'k';
    }
</script>
