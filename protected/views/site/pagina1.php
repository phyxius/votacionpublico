<button type="button" class="btn btn-info btn-lg siguiente" onclick="pagina2()">
	Siguiente
</button>

<div class="row titulo text-center">
	<h3>Seleccione sus 3 productos favoritos en orden, partiendo por el que más le gustó.</h3>
	<h1>¿Cuál es su PRIMER producto favorito?</h1>
</div>
<div class="row after-titulo">
	<?php foreach($equipos as $equipo):
	?>
	<?php $this -> renderPartial('/site/thumbnail-product', array(
			'equipo' => $equipo,
			'pregunta' => 1
		));
	?>
	<?php endforeach; ?>
</div>

<script>
	$('.siguiente').hide();
	$(document).scrollTop(0);
	
	function pagina2() {
			$('#pagina').load('<?php echo Yii::app()->createAbsoluteUrl('site/pagina2');?>?primer='+$('#primero').val());
		}
</script>