<button type="button" class="btn btn-info btn-lg anterior" onclick="pagina2()">
	Anterior
</button>

<button type="button" class="btn btn-info btn-lg siguiente" onclick="pagina4()">
	Siguiente
</button>

<div class="row titulo text-center">
	<h1>¿Cuál es su TERCER producto favorito?</h1>
</div>
<div class="row after-titulo">
	<?php foreach($equipos as $equipo):	?>
	<?php $this -> renderPartial('/site/thumbnail-product', array('equipo' => $equipo,'pregunta'=>3)); ?>
	<?php endforeach; ?>
</div>

<script>
	$('.siguiente').hide();
	$(document).scrollTop(0);
	function pagina4() {
		$('#pagina').load('<?php echo Yii::app()->createAbsoluteUrl('site/pagina4');?>');
	}
	
	function pagina2() {
			$('#pagina').load('<?php echo Yii::app()->createAbsoluteUrl('site/pagina2');?>?primer='+$('#primero').val());
		}
	
</script>