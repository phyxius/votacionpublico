<?php
/* @var $this SiteController */
/* @var $error array */

$this -> pageTitle = Yii::app() -> name . ' - Error';
$this -> breadcrumbs = null;
?>
<div class="text-center">
	<h1>Lo Sentimos! ha ocurrido un error.</h1>
	<h2>Error <?php echo $code; ?></h2>
	
	<div class="error">
	Por favor que su guía contacte con el encargado.
	</div>
</div>