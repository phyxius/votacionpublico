<?php

class SiteController extends Controller {

	/**
	 * Accion que controla la respuesta del formulario.
	 *
	 * En caso de que se hagan (como paso el 2012) 2 ferias juntas, el sistema realizará sólo una encuesta para ambos años y preguntando los
	 * mejores 3 productos pero de un listado de los productos de todas las ferias.
	 */
	public function actionIndex() {
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$periodo = Periodo::model() -> activos() -> find();
		
		if(!isset($periodo))
		{
			$this -> render('votacion_cerrada');
			die;
		}
		
		if (!isset($_GET['rut']) || trim($_GET['rut']) == '')
		{
			$this -> render('login');
		} else
		{
			$rut = $_GET['rut'];

			$periodo_id = Periodo::model() -> activos() -> find() -> id;

			$votante = Votante::model() -> find('rut = :rut AND periodo_id = :periodo', array(
				':rut' => $rut,
				':periodo' => $periodo_id
			));
			
			if (isset($votante))
			{
				$voto = RespuestaPublico::model()->exists('votante_id = :id',array(':id'=>$votante->id));
				if(isset($voto))
				{
					$this -> render('ya_voto');
					die;
				}
			}
			if (isset($_POST['pregunta']))
			{
				$preguntas = $_POST['pregunta'];

				$votante = new Votante;
				$votante -> rut = $rut;
				$votante -> periodo_id = $periodo_id;
				$votante -> save();

				//Pregunta 1
				$respuesta = new RespuestaPublico;
				$respuesta -> votante_id = $votante -> getPrimaryKey();
				$respuesta -> pregunta_id = 21;
				$respuesta -> alternativa_id = $preguntas[1];
				$respuesta -> save();

				//Pregunta 2
				$respuesta = new RespuestaPublico;
				$respuesta -> votante_id = $votante -> getPrimaryKey();
				$respuesta -> pregunta_id = 22;
				$respuesta -> alternativa_id = $preguntas[2];
				$respuesta -> save();

				//Pregunta 3
				$respuesta = new RespuestaPublico;
				$respuesta -> votante_id = $votante -> getPrimaryKey();
				$respuesta -> pregunta_id = 23;
				$respuesta -> alternativa_id = $preguntas[3];
				$respuesta -> save();

				//Pregunta 4
				$respuesta = new RespuestaPublico;
				$respuesta -> votante_id = $votante -> getPrimaryKey();
				$respuesta -> pregunta_id = 24;
				$respuesta -> alternativa_id = $preguntas[4];
				$respuesta -> save();

				//Pregunta 5
				$respuesta = new RespuestaPublico;
				$respuesta -> votante_id = $votante -> getPrimaryKey();
				$respuesta -> pregunta_id = 25;
				$respuesta -> alternativa_id = $preguntas[5];
				$respuesta -> save();

				$this -> render('agradecimientos');
			} else
			{
				$equipos = Equipo::model() -> activos() -> findAll();
				shuffle($equipos);
				$this -> render('index', array('equipos' => $equipos));
			}
		}
	}

	public function actionPagina2($primer) {

		$periodosActivos = Periodo::model() -> activos() -> findAll();
		if (isset($periodosActivos))
		{
			$equipos = Equipo::model() -> activos() -> findAll();
			shuffle($equipos);
		}

		foreach ($equipos as $key => $equipo)
		{
			if ($equipo -> id == $primer)
				unset($equipos[$key]);
		}
		$equipos = array_values($equipos);

		$this -> renderPartial('pagina2', array('equipos' => $equipos));
	}

	public function actionPagina3($primer, $segundo) {

		$periodosActivos = Periodo::model() -> activos() -> findAll();
		if (isset($periodosActivos))
		{
			$equipos = Equipo::model() -> activos() -> findAll();
			shuffle($equipos);
		}

		foreach ($equipos as $key => $equipo)
		{
			if ($equipo -> id == $primer)
				unset($equipos[$key]);
			if ($equipo -> id == $segundo)
				unset($equipos[$key]);
		}
		$equipos = array_values($equipos);

		$this -> renderPartial('pagina3', array('equipos' => $equipos));
	}

	public function actionPagina4() {

		$pregunta4 = Preguntas::model() -> findByPk(24);
		$pregunta5 = Preguntas::model() -> findByPk(25);
		$this -> renderPartial('pagina4', array(
			'pregunta4' => $pregunta4,
			'pregunta5' => $pregunta5
		));
	}

	public function actionResultados() {

		/**
		 *
		 * Alternativas aplican como id en la tabla Alternativas sólo para las preguntas 4 y 5
		 * Para las preguntas 1 - 2 - 3 alternativa_id representa el ID en la tabla equipos del equipo elegido como favorito.
		 *
		 */
		$puntajes;

		$respuestas = RespuestaPublico::model() -> activos() -> findAll();

		$cantidadVotos = Votante::model() -> activos() -> count();

		foreach ($respuestas as $respuesta)
		{
			//Primera opción
			if ($respuesta -> pregunta_id == 21)
			{
				if (!isset($puntajes[$respuesta -> alternativa_id]))
					$puntajes[$respuesta -> alternativa_id] = 3;
				else
					$puntajes[$respuesta -> alternativa_id] += 3;
			}
			//Segunda opción
			if ($respuesta -> pregunta_id == 22)
			{
				if (!isset($puntajes[$respuesta -> alternativa_id]))
					$puntajes[$respuesta -> alternativa_id] = 2;
				else
					$puntajes[$respuesta -> alternativa_id] += 2;
			}
			//Tercera opción
			if ($respuesta -> pregunta_id == 23)
			{
				if (!isset($puntajes[$respuesta -> alternativa_id]))
					$puntajes[$respuesta -> alternativa_id] = 1;
				else
					$puntajes[$respuesta -> alternativa_id] += 1;
			}
		}

		if (!empty($puntajes))
		{
			arsort($puntajes);

			$this -> render('resultados', array(
				'puntajes' => $puntajes,
				'cantidadVotos' => $cantidadVotos
			));
		} else
		{
			$this -> render('resultados', array(
				'cantidadVotos' => $cantidadVotos
			));
		}
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app() -> errorHandler -> error)
		{
			if (Yii::app() -> request -> isAjaxRequest)
				echo $error['message'];
			else
				$this -> render('error', $error);
		}
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionGetRut() {
		$rut = Votante::model() -> activos() -> findAll();
		shuffle($rut);
		$this -> render('get_rut', array('rutArray' => $rut));
	}

}
