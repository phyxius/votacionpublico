<div class="row" style="padding-bottom: 50px;">
	<div class="col-sm-12">
		<h1>Resultados de la votación</h1>
		<h3>Cantidad de votos: <?php echo $cantidadVotos; ?>
		</h3>
		<?php if(isset($puntajes)):?>
		<div class="panel panel-primary	">
			<!-- Default panel contents -->
			<div class="panel-heading">
				<h3>Todos los equipos</h3>
			</div>
			<div class="panel-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Equipo</th><th>Proyecto</th><th>Campus</th><th class="text-center">Puntaje</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($puntajes as $equipo_id => $puntos)
					{
						$equipo = Equipo::model()->findByPk($equipo_id);
					?>
					<tr>
						<td><?php echo $equipo -> preempresa; ?></td><td><?php echo $equipo -> proyecto; ?></td><td><?php echo $equipo -> sede -> nombre; ?></td><td class="text-center"><?php echo $puntos; ?></td>
					</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="panel panel-primary">
			<!-- Default panel contents -->
			<div class="panel-heading">
				<h3>Casa Central</h3>
			</div>
			<div class="panel-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Equipo</th><th>Proyecto</th><th>Campus</th><th class="text-center">Puntaje</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($puntajes as $equipo_id => $puntos)
					{
						$equipo = Equipo::model()->findByPk($equipo_id);
						if($equipo->sede_id == 1)
						{
					?>
					<tr>
						<td><?php echo $equipo -> preempresa; ?></td><td><?php echo $equipo -> proyecto; ?></td><td><?php echo $equipo -> sede -> nombre; ?></td><td class="text-center"><?php echo $puntos; ?></td>
					</tr>
					<?php 
						}
					} ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="panel panel-primary">
			<!-- Default panel contents -->
			<div class="panel-heading">
				<h3>San Joaquín</h3>
			</div>
			<div class="panel-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Equipo</th><th>Proyecto</th><th>Campus</th><th class="text-center">Puntaje</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($puntajes as $equipo_id => $puntos)
					{
						$equipo = Equipo::model()->findByPk($equipo_id);
						if($equipo->sede_id == 2)
						{
					?>
					<tr>
						<td><?php echo $equipo -> preempresa; ?></td><td><?php echo $equipo -> proyecto; ?></td><td><?php echo $equipo -> sede -> nombre; ?></td><td class="text-center"><?php echo $puntos; ?></td>
					</tr>
					<?php 
						}
					} ?>
					</tbody>
				</table>
			</div>
		</div>
		<?php endif;?>
	</div>
</div>
<script>
setTimeout(function(){
   window.location.reload(1);
}, 30000);

</script>