<button type="button" class="btn btn-info btn-lg anterior" onclick="pagina1()">
	Anterior
</button>

<button type="button" class="btn btn-info btn-lg siguiente" onclick="pagina3()">
	Siguiente
</button>

<div class="row titulo text-center">
	<h1>¿Cuál es su SEGUNDO producto favorito?</h1>
</div>
<div class="row after-titulo">
	<?php foreach($equipos as $equipo):
	?>
	<?php $this -> renderPartial('/site/thumbnail-product', array(
			'equipo' => $equipo,
			'pregunta' => 2
		));
	?>
	<?php endforeach; ?>
</div>

<script>

	$(document).scrollTop(0);
	$('.siguiente').hide();
	function pagina3() {
		$('#pagina').load('<?php echo Yii::app() -> createAbsoluteUrl('site/pagina3'); ?>?primer='+$('#primero').val()+'&segundo='+$('#segundo').val());
	}
	
	function pagina1() {
		location.reload();
	}
	
</script>