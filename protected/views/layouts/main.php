<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="en" />

		<!-- Loading Bootstrap -->
	    <?php Yii::app() -> clientScript -> registerCssFile(Yii::app() -> baseUrl . '/css/bootstrap.min.css'); ?>
	    <?php  Yii::app() -> clientScript -> registerCssFile(Yii::app() -> baseUrl . '/css/style.css'); ?>
	    <?php  Yii::app() -> clientScript -> registerScriptFile(Yii::app() -> baseUrl . '/js/jquery-1.11.0.min.js'); ?>
	    <?php  Yii::app() -> clientScript -> registerScriptFile(Yii::app() -> baseUrl . '/js/jquery-ui.min.js'); ?>
	    <?php  Yii::app() -> clientScript -> registerScriptFile(Yii::app() -> baseUrl . '/js/bootstrap.min.js'); ?>
	    <?php  Yii::app() -> clientScript -> registerScriptFile(Yii::app() -> baseUrl . '/js/jquery.mask.min.js'); ?>

	    
		<title><?php echo CHtml::encode($this -> pageTitle); ?></title>
	</head>
	
	<body>
	
		<header>
			<div class="container">
				<img src=" <?php echo Yii::app() -> baseUrl . "/images/logo.png"; ?> " />
			</div>
		</header>
			
		<div class="container" id="page">
				<?php echo $content; ?>
		</div>
		<footer>
			<div class="container">
				XXII Feria de Software<br/>
				Departamento de Informática<br/>
				Universidad Técnica Federico Santa María
			</div>
		</footer>
	</body>
</html>
